package branchio

import (
	"bytes"
	"encoding/json"
	"github.com/globalsign/mgo/bson"
	"io"
	"net/http"
)

var serverUrl string
var apiKey string
var presharedKey string

type EventData struct {
	TransactionId string  `bson:"transaction_id" json:"transaction_id"`
	Currency      string  `bson:"currency,omitempty" json:"currency,omitempty"`
	Revenue       float32 `bson:"revenue" json:"revenue"`
	Shipping      float32 `bson:"shipping" json:"shipping"`
	Tax           float32 `bson:"tax" json:"tax"`
	Coupon        string  `bson:"coupon,omitempty" json:"coupon,omitempty"`
	Affiliation   string  `bson:"affiliation,omitempty" json:"affiliation,omitempty"`
	Description   string  `bson:"description,omitempty" json:"description,omitempty"`
}

type UserDeviceInfo struct {
	OS                string `bson:"os" json:"os"`
	OSVersion         int    `bson:"os_version,omitempty" json:"os_version,omitempty"`
	Environment       string `bson:"environment,omitempty" json:"environment,omitempty"`
	AAID              string `bson:"aaid" json:"aaid"`
	AndroidId         string `bson:"android_id" json:"android_id"`
	LimitAdTracking   bool   `bson:"limit_ad_tracking,omitempty" json:"limit_ad_tracking,omitempty"`
	DeveloperIdentity string `bson:"developer_identity" json:"developer_identity"`
	Country           string `bson:"country,omitempty" json:"country,omitempty"`
	Language          string `bson:"language,omitempty" json:"language,omitempty"`
	LocalIp           string `bson:"local_ip,omitempty" json:"local_ip,omitempty"`
	IP                string `bson:"ip" json:"ip"`
	Brand             string `bson:"brand,omitempty" json:"brand,omitempty"`
	AppVersion        string `bson:"app_version,omitempty" json:"app_version,omitempty"`
	Model             string `bson:"model,omitempty" json:"model,omitempty"`
	ScreenDPI         int    `bson:"screen_dpi,omitempty" json:"screen_dpi,omitempty"`
	ScreenHeight      int    `bson:"screen_height,omitempty" json:"screen_height,omitempty"`
	ScreenWidth       int    `bson:"screen_width,omitempty" json:"screen_width,omitempty"`
}

type ContentItem struct {
	ContentSchema       string      `bson:"$content_schema" json:"$content_schema"`
	Title               string      `bson:"$og_title" json:"$og_title"`
	Description         string      `bson:"$og_description" json:"$og_description"`
	ImageUrl            string      `bson:"$og_image_url" json:"$og_image_url"`
	CanonicalIdentifier string      `bson:"$canonical_identifier" json:"$canonical_identifier"`
	PubliclyIndexable   bool        `bson:"$publicly_indexable" json:"$publicly_indexable"`
	Price               float64     `bson:"$price" json:"$price"`
	LocallyIndexable    bool        `bson:"$locally_indexable" json:"$locally_indexable"`
	Quantity            int         `bson:"$quantity" json:"$quantity"`
	SKU                 int         `bson:"$sku" json:"$sku"`
	ProductName         string      `bson:"$product_name" json:"$product_name"`
	ProductBrand        string      `bson:"$product_brand" json:"$product_brand"`
	ProductCategory     string      `bson:"$product_category" json:"$product_category"`
	ProductVariant      string      `bson:"$product_variant" json:"$product_variant"`
	RatingAverage       float32     `bson:"$rating_average" json:"$rating_average"`
	RatingCount         int         `bson:"$rating_count" json:"$rating_count"`
	RatingMax           float32     `bson:"$rating_max" json:"$rating_max"`
	CreationTimeStamp   int         `bson:"$creation_timestamp" json:"$creation_timestamp"`
	ExpiryDate          int         `bson:"$exp_date" json:"$exp_date"`
	KeyWords            []string    `bson:"$keywords" json:"$keywords"`
	AddressStreet       string      `bson:"$address_street" json:"$address_street"`
	AddressCity         string      `bson:"$address_city" json:"$address_city"`
	AddressRegion       string      `bson:"$address_region" json:"$address_region"`
	AddressCountry      string      `bson:"$address_country" json:"$address_country"`
	AddressPostal       int32       `bson:"$address_postal_code" json:"$address_postal_code"`
	AddressLatitude     float64     `bson:"$latitude" json:"$latitude"`
	AddressLongitude    float64     `bson:"$longitude" json:"$longitude"`
	ImageCaptions       []string    `bson:"$image_captions" json:"$image_captions"`
	Condition           string      `bson:"$condition" json:"$condition"`
	CustomFields        interface{} `bson:"$custom_fields" json:"$custom_fields"`
}

type BranchResponse struct {
	BranchViewEnabled bool   `bson:"branch_view_enabled" json:"branch_view_enabled"`
	Error             bson.M `bson:"error" json:"error"`
}

func Init(api_key string, preshared string) {
	serverUrl = "https://api2.branch.io"
	apiKey = api_key
	presharedKey = preshared
}

func SendRequest(method string, url string, body io.Reader, headers map[string]string) (*http.Response, error) {
	url = serverUrl + url
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	for name, value := range headers {
		req.Header.Add(name, value)
	}

	var client http.Client
	return client.Do(req)
}

//returns response given by branch io api
func PushCommerceEvent(customData interface{}, info UserDeviceInfo, overrideIp string, data EventData, items []ContentItem) (result BranchResponse, err error) {
	var eventName = "android_purchase_event"

	infoWithKey := struct {
		UserDeviceInfo
		PreSharedKey string `json:"referral_source"`
	}{
		info,
		presharedKey,
	}

	body := bson.M{
		"name":                 "PURCHASE",
		"customer_event_alias": eventName,
		"user_data":            infoWithKey,
		"event_data":           data,
		"branch_key":           apiKey,
	}

	if customData != nil {
		body["custom_data"] = customData
	}

	if len(items) != 0 {
		body["content_items"] = items
	}

	byteData, err := json.Marshal(body)
	if err != nil {
		return result, err
	}

	headers := map[string]string{
		"Content-Type":  "application/json",
		"X-IP-Override": overrideIp,
	}

	res, err := SendRequest("POST", "/v2/event/standard", bytes.NewBuffer(byteData), headers)
	if err != nil {
		return result, err
	}

	err = json.NewDecoder(res.Body).Decode(&result)
	return result, err
}
